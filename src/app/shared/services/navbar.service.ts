import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class NavbarService {
  private navbarTitleObservable = new BehaviorSubject<string>("");

  constructor() {}

  navbarTItleObserver(title?: string): Observable<string> {
    if (title) {
      this.navbarTitleObservable.next(title);
      return;
    }
    return this.navbarTitleObservable.asObservable();
  }
}
