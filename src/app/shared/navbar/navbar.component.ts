import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NavbarService } from "../services/navbar.service";

interface INavbar {
  name: string;
  link: string[];
}

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit {
  navbarItem: INavbar[];
  isFirst = true;
  constructor(
    private route: ActivatedRoute,
    private navbarService: NavbarService
  ) {}

  ngOnInit(): void {
    this.getNavbarItem();
    if (this.isFirst) {
      this.getTitle();
    }
  }

  getTitle() {
    const currentPathSplit = location.pathname.split("/");
    const childRoute = currentPathSplit[currentPathSplit.length - 1];
    const currentTitle = this.navbarItem.filter((d) => {
      const link = d.link[0].split("/");
      if (link[link.length - 1] === childRoute) {
        return d;
      }
    })[0].name;
    this.navbarService.navbarTItleObserver(currentTitle);
  }

  OnTitle(d: INavbar) {
    this.navbarService.navbarTItleObserver(d.name);
  }

  getNavbarItem() {
    this.navbarItem = [
      {
        name: "Angular",
        link: ["/dashboard/angular"],
      },
      {
        name: "Firebase",
        link: ["/firebase"],
      },
      {
        name: "Javascript",
        link: ["/dashboard/javascript"],
      },
      {
        name: "Node JS",
        link: ["/dashboard/node-js"],
      },
      {
        name: "Mongodb",
        link: ["/dashboard/mongodb"],
      },
      {
        name: "CSS",
        link: ["/dashboard/css"],
      },
      {
        name: "Interview Question",
        link: ["/dashboard/interview-question"],
      },
    ];
  }
}
