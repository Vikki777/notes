import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../shared/services/navbar.service';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss']
})
export class DashboardLayoutComponent implements OnInit {
  title: string;
  constructor(private navbarService: NavbarService) { }

  ngOnInit(): void {
    this.navbarService.navbarTItleObserver()
      .subscribe(d => {
        this.title = d;
      });
  }

}
