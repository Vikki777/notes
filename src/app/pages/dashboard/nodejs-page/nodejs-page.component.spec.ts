import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodejsPageComponent } from './nodejs-page.component';

describe('NodejsPageComponent', () => {
  let component: NodejsPageComponent;
  let fixture: ComponentFixture<NodejsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodejsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodejsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
