import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AngularPageComponent } from "./angular-page/angular-page.component";
import { CssPageComponent } from "./css-page/css-page.component";
import { JavascriptPageComponent } from "./javascript-page/javascript-page.component";
import { MongodbPageComponent } from "./mongodb-page/mongodb-page.component";
import { NodejsPageComponent } from "./nodejs-page/nodejs-page.component";
import { InterviewQuestionComponent } from "./interview-question/interview-question.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "angular",
  },
  {
    path: "node-js",
    component: NodejsPageComponent,
  },
  {
    path: "javascript",
    component: JavascriptPageComponent,
  },
  {
    path: "mongodb",
    component: MongodbPageComponent,
  },
  {
    path: "angular",
    pathMatch: "full",
    component: AngularPageComponent,
  },
  {
    path: "css",
    component: CssPageComponent,
  },
  {
    path: "interview-question",
    component: InterviewQuestionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
