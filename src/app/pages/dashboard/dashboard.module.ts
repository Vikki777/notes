import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { AngularPageComponent } from "./angular-page/angular-page.component";
import { JavascriptPageComponent } from "./javascript-page/javascript-page.component";
import { MongodbPageComponent } from "./mongodb-page/mongodb-page.component";
import { NodejsPageComponent } from "./nodejs-page/nodejs-page.component";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { CssPageComponent } from './css-page/css-page.component';
import { InterviewQuestionComponent } from './interview-question/interview-question.component';

@NgModule({
  declarations: [
    AngularPageComponent,
    JavascriptPageComponent,
    MongodbPageComponent,
    NodejsPageComponent,
    CssPageComponent,
    InterviewQuestionComponent,
  ],
  imports: [CommonModule, DashboardRoutingModule, NgxExtendedPdfViewerModule],
})
export class DashboardModule {}
