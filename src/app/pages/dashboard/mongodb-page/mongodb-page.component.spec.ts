import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MongodbPageComponent } from './mongodb-page.component';

describe('MongodbPageComponent', () => {
  let component: MongodbPageComponent;
  let fixture: ComponentFixture<MongodbPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MongodbPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MongodbPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
