import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FirebasePageRoutingModule } from "./firebase-page-routing.module";
import { FirebaseMaterialComponent } from "./firebase-material/firebase-material.component";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";

@NgModule({
  declarations: [FirebaseMaterialComponent],
  imports: [
    CommonModule,
    FirebasePageRoutingModule,
    NgxExtendedPdfViewerModule,
  ],
})
export class FirebasePageModule {}
