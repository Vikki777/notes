import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirebaseMaterialComponent } from './firebase-material/firebase-material.component';


const routes: Routes = [
  {
    path: "",
    component: FirebaseMaterialComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FirebasePageRoutingModule { }
