import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirebaseMaterialComponent } from './firebase-material.component';

describe('FirebaseMaterialComponent', () => {
  let component: FirebaseMaterialComponent;
  let fixture: ComponentFixture<FirebaseMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirebaseMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirebaseMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
