import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-firebase-material",
  templateUrl: "./firebase-material.component.html",
  styleUrls: ["./firebase-material.component.scss"],
})
export class FirebaseMaterialComponent implements OnInit {
  pdfPath = "assets/pdf-files/firebase/node-firebase.pdf";
  constructor() {}

  ngOnInit(): void {}
}
